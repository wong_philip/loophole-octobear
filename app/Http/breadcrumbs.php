<?php

//Home
Breadcrumbs::register('index', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});
// Home > About
Breadcrumbs::register('about', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('About Us', route('about-us'));
});
// Home > Our Service
Breadcrumbs::register('our-service', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('What We Provide', route('our-service'));
});
// Home > News
Breadcrumbs::register('news', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('News', route('news'));
});
// Home > New Patients
Breadcrumbs::register('new-patient', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('New Patients', route('new-patient'));
});
// Home > Chiropractic and You
Breadcrumbs::register('chiropractic-info', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Chiropractic and You', route('chiropractic-info'));
});
// Home > Contact Us
Breadcrumbs::register('contact-us', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Contact Us', route('contact-us'));
});