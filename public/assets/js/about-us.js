/**
 * Created by lawrencelaw on 28/3/15.
 */
$(function () {
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-pills a[href=#' + url.split('#')[1] + ']').tab('show');
    }

    $('.nav-pills a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
        window.scrollTo(0, 0);
    });

    $("#navmeet").click(function () {
        $('#myTab a[href="#meet"]').tab('show');
        window.scrollTo(0, 0);
    });
    $("#navtest").click(function () {
        $('#myTab a[href="#testimonials"]').tab('show');
        window.scrollTo(0, 0);
    });

    $("#doctor-pic-1").click(function () {
        $("#doctor1").toggle(1000);
    });
    $("#doctor-pic-2").click(function () {
        $("#doctor2").toggle(1000);
    });
    $("#doctor-pic-3").click(function () {
        $("#doctor3").toggle(1000);
    });
    $("#doctor-pic-4").click(function () {
        $("#doctor4").toggle(1000);
    });
    $("#doctor4-back").click(function () {
        $("#doctor4-backquote0").toggle(500);
        $("#doctor4-backquote1").toggle(500);
    });
});