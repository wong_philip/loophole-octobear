@extends('app')

@section('title','Contact Us')

@section('breadcrumbs', Breadcrumbs::render('contact-us'))

@section('content')
    <h3>@yield('title')</h3>

    <div class="row">
        <div class="col-md-8">
            <h4>Details</h4>
            <address>
                Room 1201B<br/>
                12/F, Hong Kong Pacific Centre<br/>
                28 Hankow Road<br/>
                Tsim Sha Tsui<br/>
                Kowloon<br/>
                <abbr title="Phone">P:</abbr> (+852) 2721 3999
            </address>
            <button class="btn btn-xs btn-primary" id="mapsShowHide">Show Maps</button>
            <h4>Available Time for Booking</h4>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td>Day</td>
                    <td>Time</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Mondays - Fridays</td>
                    <td>10:00AM-1:30PM, 3:30PM-6:30PM</td>
                </tr>
                <tr>
                    <td>Saturday</td>
                    <td>
                        10AM-1:30PM, 3:30PM-6:00PM
                    </td>
                </tr>
                <tr>
                    <td>Sundays & Public Holidays</td>
                    <td>
                        Closed
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <div class="well" id="googleMapWell" hidden="hidden">
                <img class="img-responsive"
                     src="//maps.googleapis.com/maps/api/staticmap?center=22.297151,+114.171286&zoom=18&scale=2&size=320x240&maptype=roadmap&format=png&visual_refresh=true&markers=size:small%7Ccolor:red%7Clabel:0%7C22.297151,+114.171286"
                     alt="Google Map of 22.297151, 114.171286">
            </div>
        </div>
    </div>
@endsection

@section('bottom-js')
    <script>
        $(function () {
            $("#mapsShowHide").click(function () {
                $("#googleMapWell").toggle(1000);
            });
        });
    </script>
@endsection