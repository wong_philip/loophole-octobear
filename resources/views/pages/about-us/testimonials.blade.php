<h4>Patient Testimonials</h4>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                   aria-expanded="true"
                   aria-controls="collapseOne">
                    Female, 32 years old
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
             aria-labelledby="headingOne">
            <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi asperiores
                    atque cumque deleniti ducimus eum explicabo illo ipsa, maxime, necessitatibus
                    officiis optio placeat quos reiciendis sit suscipit vero voluptas voluptate
                    voluptates. Consequatur deserunt dolor doloribus earum eligendi est, fuga id
                    inventore ipsa maiores nisi odio officiis pariatur tempora veritatis.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                   aria-expanded="false" aria-controls="collapseTwo">
                    Male, 35 years old
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
             aria-labelledby="headingTwo">
            <div class="panel-body">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto
                    beatae consequuntur dicta eligendi eos eum explicabo itaque libero magnam nobis,
                    officiis provident rem sunt vel veniam. Adipisci aliquam at dolores est et
                    exercitationem id illo in, incidunt iste laudantium modi, nihil non numquam officiis
                    perferendis quaerat quod saepe similique sit suscipit ullam voluptate voluptatum? At
                    fuga maxime soluta?
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                   href="#collapseThree"
                   aria-expanded="false" aria-controls="collapseThree">
                    Female, 39 years old
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
             aria-labelledby="headingThree">
            <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab amet doloremque enim,
                    illum laborum provident quae quidem tempore ut voluptate.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingFour">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                   href="#collapseFour"
                   aria-expanded="false" aria-controls="collapseFour">
                    Male, 20 years old
                </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
             aria-labelledby="headingFour">
            <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At ducimus error quibusdam
                    similique voluptates! A alias, aliquid, amet architecto assumenda commodi
                    consequatur cumque debitis distinctio dolor dolorum ducimus eaque earum eos fugit
                    ipsa iste iusto maiores, nostrum omnis perspiciatis quas quia quisquam repellat
                    saepe sunt tempora ullam. Accusantium at autem cumque doloremque dolores ea eligendi
                    error eum expedita, facere facilis fuga labore magni maxime, mollitia nemo neque
                    officia omnis pariatur placeat quas quasi quisquam quod quos ratione repellendus sed
                    sit tempore tenetur vel veniam, veritatis voluptas voluptate. Alias beatae
                    consequatur esse excepturi, expedita harum inventore ipsam, perspiciatis quas, quis
                    quisquam.</p>
            </div>
        </div>
    </div>
</div>