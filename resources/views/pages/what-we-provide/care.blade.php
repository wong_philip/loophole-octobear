<h4>Chiropractic Care</h4>

<p class="lead">The following adjustment techniques are available in our practice.</p>

<h5>Manual Diversified</h5>
<p>
    The most commonly used adjustment technique by chiropractors. Diversified is characterized by a gentle
    high-velocity,
    low-amplitude thrust. Its objective is to restore proper movement and alignment of spine and joint dysfunction.
</p>

<h5>Thompson Drop</h5>
<p>
    Relies on a special table with sectional drop pieces that allow the chiropractor to use the
    patient's body weight when adjusting. It also places emphasis on strict procedural protocols.
</p>

<h5>Activator</h5>
<p>
    A light force application which is suitable for children to adults. The activator is a small handheld spring-loaded
    instrument which delivers a small impulse to the spine. It has been found to give off no more than 0.3 J of kinetic
    energy in a 3-millisecond pulse.
</p>

<h5>ArthroStim</h5>
<p>
    ArthroStim is an adjusting instrument that is an alternative to manual adjustments.
</p>

<h5>Flexion/Distraction</h5>
<p>
    A technique utilized for Lumbar Disc Herniation, in addition it is very effective for Lumbar muscular release. A
    gentle traction-ing force is applied to the spine, while the motor flexes the lower part of the table.
</p>

<h5>Shockwave Therapy</h5>
<p>
    Very effective treatment for muscular trigger point, tendonitis, tennis elbow, heel pain, frozen shoulder, and
    sports injury. This device is a combination of "radial shockwave," and "focus shockwave" technology.
</p>

<h5>Spinal Impulse</h5>
<p>
    Gentle, effective Spinal percussion device. This device is very similar to the activator, however it is motorized.
    It delivers a more uniform force, and our clients love this adjusting device!
</p>